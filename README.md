# Prérequis
- Installer WAMP sur la machine pour utiliser le projet en local avec PhpMyAdmin
- Créer la base de données "gfpfrance" (base MySQL)
- Créer un dossier gfpfranceBO2019 dans le répertoire "www" de WAMP

# Cloner le projet depuis GitLab
## En ligne de commande
- Aller dans le dossier gfpfranceBO2019 et utiliser la commande suivante avec Git Bash: "git clone https://gitlab.com/erwan-scherrer-e4/back-office.git"

## Manuellement

# Configurer la base de données et la connexion
- La connexion à la base de données se configure dans « ..\config\autoload\global.php »
- La base de données est "gfpfrance" (en minuscule et tout attaché)
- Une fois la base créer utiliser le script "script-back-office.sql" pour générer les tables et les données de test
